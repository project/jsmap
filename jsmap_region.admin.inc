<?php

/**
 * @file
 * jsmap_region editing UI.
 *
 * We make very little use of the EntityAPI interface for this - preferring instead to use
 * views. That offers more flexibility to change a UI that will, more often than not,
 * be end-user facing.
 */

/**
 * UI controller.
 */
class JsmapRegionUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults. Main reason for doing this is that
   * parent class hook_menu() is optimized for entity type administration.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
     
    
    // Add menu items to add each different type of entity.
    foreach (jsmap_get_maps() as $map) {
      $items[$this->path . '/' . $map->map .'/add'] = array(
        'title' => 'Add region to ' . $map->label,
        'page callback' => 'entity_ui_get_form',
      	'page arguments' => array($this->entityType, jsmap_region_create(array('map' => $map->map)), 'add'),
        'access callback' => 'jsmap_region_access',
        'access arguments' => array('edit', 'edit ' . $map->map),
        'file' => 'jsmap_region.admin.inc',
        'file path' => drupal_get_path('module', $this->entityInfo['module']),
        'type' => MENU_LOCAL_ACTION,
      );
      
      $items[$this->path .'/'. $map->map] = array(
        'title' => $this->entityInfo['label'] . 's in '. $map->label,
        'page callback' => 'drupal_get_form',
        'page arguments' => array($this->entityType . '_overview_form', $this->entityType, $map->map),
        'description' => 'Manage ' . $this->entityInfo['label'] . 's.',
        'access callback' => 'entity_access',
        'access arguments' => array('view', $this->entityType),
        'file' => 'includes/entity.ui.inc',
      );
    }

  

    return $items;
  }

  
  
  /**
   * Builds the entity overview form.
   */
  public function overviewForm($form, &$form_state, $bundle = NULL) {
    // By default just show a simple overview for all entities.
    $form['table'] = $this->overviewTable(array('map' => $bundle));
    return $form;
  }
  
}

/**
 * Form builder function for the overview form.
 *
 * @see EntityDefaultUIController::overviewForm()
 */
function jsmap_region_overview_form($form, &$form_state, $entity_type, $bundle = NULL) {
  return entity_ui_controller($entity_type)->overviewForm($form, $form_state, $bundle);
}

/**
 * Form callback: create or edit a jsmap_region.
 *
 * @param $jsmap_region
 *   The jsmap_region object to edit or for a create form an empty jsmap_region object
 *     with only a jsmap_region type defined.
 */
function jsmap_region_form($form, &$form_state, $jsmap_region, $op = 'edit') {

  // Add the default field elements.
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Region Name'),
    '#default_value' => isset($jsmap_region->name) ? $jsmap_region->name : '',
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => -5,
  );
  
  $form['svg_path'] = array(
    '#type' => 'textarea',
    '#title' => t('SVG path'),
    '#default_value' => isset($jsmap_region->svg_path) ? $jsmap_region->svg_path : '',
  );
  
  
  $form['interactive'] = array(
    '#type' => 'checkbox',
    '#title' => t('Make this region interactive'),
    '#default_value' => isset($jsmap_region->interactive) ? $jsmap_region->interactive : MAP_VALUE_YES,
  );
  
  $form['name_display'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display the label on this region'),
    '#default_value' => isset($jsmap_region->name_display) ? $jsmap_region->name_display : MAP_VALUE_YES,
  );
  
  $form['name_offset'] = array(
    '#type' => 'textfield',
    '#title' => t('Name offset'),
    '#description' => t('name x y offset from region center as a percentage of the map width / height'),
    '#default_value' => isset($jsmap_region->name_offset) ? $jsmap_region->name_offset : '',
  );
  
  $form['name_use_line'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display a line to the label of this region'),
    '#default_value' => isset($jsmap_region->name_use_line) ? $jsmap_region->name_use_line : MAP_VALUE_NO,
  );
  
  $form['name_line_offset'] = array(
    '#type' => 'textfield',
    '#title' => t('Line offset'),
    '#description' => t('line x y offset from region center as a percentage of the map width / height'),
    '#default_value' => isset($jsmap_region->name_line_offset) ? $jsmap_region->name_line_offset : '',
  );
  
  
  
  // Add the field related form elements.
  $form_state['jsmap_region'] = $jsmap_region;
  field_attach_form('jsmap_region', $jsmap_region, $form, $form_state);
  
  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save region'),
    '#submit' => $submit + array('jsmap_region_form_submit'),
  );

  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'jsmap_region_form_validate';
  
  return $form;
}

/**
 * Form API submit callback for the jsmap_region form.
 * 
 */
function jsmap_region_form_submit(&$form, &$form_state) {
  
  $jsmap_region = entity_ui_controller('jsmap_region')->entityFormSubmitBuildEntity($form, $form_state);
  // Save the jsmap_region and go back to the list of jsmap_regions
  
  // Add in created and changed times.
  if ($jsmap_region->is_new = isset($jsmap_region->is_new) ? $jsmap_region->is_new : 0){
    $jsmap_region->created = time();
  }

  $jsmap_region->changed = time();
  
  $jsmap_region->save();
  $form_state['redirect'] = jsmap_region_get_bundle_path($form['#bundle']);
}


function jsmap_region_get_bundle_path($bundle){
  $info = entity_get_info('jsmap_region');
  return $info['admin ui']['path'] . '/'. $bundle;
}


/**
 * Form API validate callback for the jsmap_region form
 */
function jsmap_region_form_validate(&$form, &$form_state) {
  $jsmap_region = $form_state['jsmap_region'];
  
  // Notify field widgets to validate their data.
  field_attach_form_validate('jsmap_region', $jsmap_region, $form, $form_state);
}