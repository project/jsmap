<?php
/**
 * Implements hook_views_plugins().
 */
function jsmap_views_plugins() {
  return array(
    'style' => array(
      'jsmap' => array(
        'title' => t('Raphael JS Map'),
        'help' => t('Display the results as a map.'),
        'handler' => 'jsmap_plugin_style_jsmap',
        'uses options' => TRUE,
        'uses row plugin' => TRUE,
        'uses grouping' => TRUE,
        'type' => 'normal',
        'parent' => 'list',
        'path' => drupal_get_path('module', 'jsmap'),
        'theme' => 'views_jsmap',
        'theme path' => drupal_get_path('module', 'jsmap') . '/theme',
        'theme file' => 'jsmap.theme.inc',
      ),
    ),
  );
}


function jsmap_views_data_alter(&$data){
  //dsm($data);
  
  $data['field_data_field_region']['field_region_id']['relationship'] = array(
    'handler' => 'views_handler_relationship',
    'base' => 'jsmap_region',
    'base field' => 'id',
    'label' => 'jsMap region from field_region'
  );
}