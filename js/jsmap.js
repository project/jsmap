(function ($) {

Drupal.behaviors.map = {
  attach: function(context, settings) {

    var maps = settings.jsmaps;

    for(var map_i in maps){
    	var map = maps[map_i];
    	
    	//skip this map if the container div doesn't exist
    	if($('#' + map.jsmap_id,context).length == 0) continue;
    	
    	//create map canvas and regionset to contain everything that's drawn
    	var map_div = $('#' + map.jsmap_id,context);
    	var r = Raphael(map.jsmap_id, "100%","100%");
    	map.regionset = r.set();//all regions
    	map.mapset = r.set();//everything drawn on the canvas
    	map.labelset = r.set();//just the label elements
    	map.lineset = r.set();//just the line elements
	    
	    //loop through all regions
	    for (var country in map.regions) {
	      var regiongroup = r.set();//all elements pertaining to this region (the region, text, and line)
	    	
	      //draw region and add it to the set
	      var obj = r.path(map.regions[country].path);
	      map.regionset.push(obj);
	      map.mapset.push(obj);
	      regiongroup.push(obj);
	
	      //set path attributes
	      obj.attr(map.defaults.pathDefaults);
	      
	      //get the current region's bounding box
	      var bbox = obj.getBBox();
	
	      //set the background of the region -- prefer the data-driven background, fall back to default
	      var regionBG = map.regions[country].fill || map.defaults.pathDefaults.fill;
	      obj.attr({'fill': regionBG});
	      
	      //decide if we should show labels on the map
	      var showNames = map.defaults.general.showNames;
	      if(showNames == 2){
	    	  showNames = map.regions[country].name_display;
	      }
	      showNames = (showNames == 1);
	      
	      //decide if the region should be interactive
	      var interactive = map.defaults.pathDefaults.interactive;
	      if(interactive == 2){
	    	  interactive = map.regions[country].interactive;
	      }
	      interactive = (interactive == 1);
	      
	      // draw name
	      if (showNames) {
	    	  var nameOffset = map.regions[country].name_offset ? map.regions[country].name_offset.split(' ') : [0,0];
			  var namePosX = bbox.x + bbox.width/2 + parseFloat(nameOffset[0]);
			  var namePosY = bbox.y + bbox.height/2 + parseFloat(nameOffset[1]);		
			  var text = r.text(namePosX, namePosY, map.regions[country].name);
			  text.attr({"font-size": map.defaults.general.fontSize});
			  map.labelset.push(text);
			  map.mapset.push(text);
			  regiongroup.push(text);
			    
			  // draw line to name, if appropriate
			  if (parseInt(map.regions[country].name_use_line)) {
				  var lineOffset = map.regions[country].name_line_offset ? map.regions[country].name_line_offset.split(' ') : [0,0];
			      var linePosX = bbox.x + bbox.width/2 + parseFloat(lineOffset[0]);
			      var linePosY = bbox.y + bbox.height/2 + parseFloat(lineOffset[1]);
			      var line = r.path('m '+ linePosX +' '+ linePosY +' l '+ (bbox.x - linePosX - 2) +' '+ ((bbox.y + bbox.height/2) - linePosY));
			      map.mapset.push(line);
			      map.lineset.push(line);
			      regiongroup.push(line);
			  }
			}
	      
	      if (interactive) {
	    	(function(){
	    		var c = country;
	    		var m = map;
	    		var m_d = map_div;
	    		var local_regionBG = regionBG;
	    		var region = obj;
	    		var t = text;
	    		var l = line;
	    		
	    		var data_div = $('#' + m.regions[c].data_div_id,context).hide();//get the corresponding data div

	    		//make region go to link on click
	    		obj.click(function() {
		          window.location = m.regions[c].url;
		        });
	    		
	    		//change color on hover and add popup to page
	    		regiongroup.hover(function(e){		
		        	data_div.show();					
					region.animate({
						fill: m.defaults.pathDefaults['fill-hover']
					}, 200);
								
				}, function(e){
					data_div.hide();
					region.animate({
						fill: local_regionBG
					}, 200);
					
					$('.point',context).remove();
		        });
	    	})();
	        
	      }
	    }
	    
	    
	    //get the entire map's bounding box
	    bbox = map.mapset.getBBox();
	    
	    var max_x = (bbox.x + bbox.width);
	    var max_y = (bbox.y + bbox.height);
	    var min_x = bbox.x;
	    var min_y = bbox.y;
	    var height = max_y - min_y;
	    var width = max_x - min_x;
	    var scale = map.defaults.general.scale;
	    
	    if(!(scale > 0)){
	    	var target_height = map.defaults.general.max_height;
	    	var target_width = map.defaults.general.max_width;
	    	var map_ratio = width / height;
	    	var target_ratio = target_width / target_height;
	    	
	    	//map is wider than target so base scale on width
	    	if(map_ratio > target_ratio){
	    		scale = target_width / width;
	    	}
	    	//map is taller than target so  base scale on height
	    	else{
	    		scale = target_height / height;
	    	}
	    }
	    
	    max_x = max_x * scale;
	    max_y = max_y * scale;
	    min_x = min_x * scale;
	    min_y = min_y * scale;
	    
	    //scale everything
	    map.mapset.scale(scale, scale, 0, 0);
	    
	    //move the elements to the top left portion of the canvas so that all are visible
	    map.mapset.translate(0 - min_x + 1, 0 - min_y + 1);
	    
	    //resize the canvas to just fit the contents
	    r.setSize(max_x - min_x + 2,max_y - min_y + 2);
	    	    
	    //move the regions to the back
	    map.regionset.toBack();
    }
  },

  detach: function(context, settings) {

  }
}

}(jQuery));
