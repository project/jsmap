<?php

/**
 * Style plugin to render each item in a map
 *
 * @ingroup views_style_plugins
 */
class jsmap_plugin_style_jsmap extends views_plugin_style {

  // Set default options
  function option_definition() {
    $options = parent::option_definition();
    
    $options['region_field'] = array('default' => '');
    $options['metaregion_group'] = array('default' => '');
    $options['custom_color_field'] = array('default' => '');
    $options['scale'] = array('default' => '1');
    $options['max_width'] = array('default' => '');
    $options['max_height'] = array('default' => '');
    $options['display_labels'] = array('default' => MAP_VALUE_DEFAULT);
    $options['label_size'] = array('default' => '10');
    $options['region_color'] = array('default' => '#FFFFFF');
    $options['interactive'] = array('default' => MAP_VALUE_DEFAULT);
    $options['region_hover_color'] = array('default' => '#1669AD');
    $options['stroke_color'] = array('default' => '#3899E6');
    $options['stroke_width'] = array('default' => '1');
    
    return $options;
  }

  // Build the settings form for the view.
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    
    //select field to use for region data
    //@todo limit this list only to the right types of fields
    $options = array('' => t('- None -'));
    $options += $this->display->handler->get_field_labels();
    $yesno_options = array(
      MAP_VALUE_DEFAULT => 'Determined by the region',
      MAP_VALUE_YES => 'Always',
      MAP_VALUE_NO => 'Never',
    );
    
    // If there are no fields, we can't choose a region field
    if (count($options) > 1) {
      $form['region_field'] = array(
        '#type' => 'select',
        '#title' => t('Region field'),
        '#options' => $options,
        '#default_value' => $this->options['region_field'],
        '#description' => t('The region field which will provide shape data for this map'),
      );
      
      $form['scale'] = array(
      	'#type' => 'textfield',
        '#title' => t('Map scale (1 is 100%)'),
        '#default_value' => $this->options['scale'],
      );
      
      $form['max_width'] = array(
      	'#type' => 'textfield',
        '#title' => t('Max width'),
        '#default_value' => $this->options['max_width'],
      );
      
      $form['max_height'] = array(
      	'#type' => 'textfield',
        '#title' => t('Max height'),
        '#default_value' => $this->options['max_height'],
      );
      
      $form['display_labels'] = array(
        '#type' => 'select',
        '#options' => $yesno_options,
        '#title' => t('Display region labels'),
        '#default_value' => $this->options['display_labels'],
      ); 
      
      $form['label_size'] = array(
      	'#type' => 'textfield',
        '#title' => t('Region label font size'),
        '#default_value' => $this->options['label_size'],
      );
      
      $form['region_color'] = array(
      	'#type' => 'textfield',
        '#title' => t('Region background color'),
        '#default_value' => $this->options['region_color'],
      );
      
      $form['custom_color_field'] = array(
        '#type' => 'select',
        '#title' => t('Custom color field'),
        '#options' => $options,
        '#default_value' => $this->options['custom_color_field'],
        '#description' => t('Optional field containing per-region color information in hexadecimal format'),
      );
      
      $form['interactive'] = array(
        '#type' => 'select',
        '#options' => $yesno_options,
        '#title' => t('Make region interactive'),
        '#default_value' => $this->options['interactive'],
      ); 
      
      $form['region_hover_color'] = array(
      	'#type' => 'textfield',
        '#title' => t('Region background color on hover'),
        '#default_value' => $this->options['region_hover_color'],
      );
      
      $form['stroke_color'] = array(
      	'#type' => 'textfield',
        '#title' => t('Stroke (line) color'),
        '#default_value' => $this->options['stroke_color'],
      );
      
      $form['stroke_width'] = array(
      	'#type' => 'textfield',
        '#title' => t('Stroke (line) width'),
        '#default_value' => $this->options['stroke_width'],
      );
      
      $form['metaregion_group'] = array(
        '#type' => 'select',
        '#title' => t('Meta Region Group'),
        '#options' => $options,
        '#default_value' => $this->options['metaregion_group'],
        '#description' => t('Optionally group map data on a field to create custom meta regions'),
      );
    }
    
  }
  
  /**
   * Render the display in this style.
   */
  function render() {
    if ($this->uses_row_plugin() && empty($this->row_plugin)) {
      debug('views_plugin_style_default: Missing row plugin');
      return;
    }

    // Group the rows according to the grouping field, if specified.
    $sets = $this->render_grouping($this->view->result, $this->options['grouping']);

    // Render each group separately and concatenate.
    $output = '';
    foreach ($sets as $title => $records) {
      $rows = array();
      $map_regions = array();
      
      //Group the region data if specified
      $metaregions = $this->render_grouping($records, $this->options['metaregion_group']);
      
      //If meta region grouping not specified then make each region its own metaregion
      if(empty($this->options['metaregion_group'])){
       $temp_metaregions = $metaregions;
       unset($metaregions);
       foreach($temp_metaregions[''] as $key => $region){
         $metaregions[$key] = array($key => $region);
       }
      }
      
      foreach($metaregions as $key => $metaregion){
        foreach ($metaregion as $row_index => $row) {
          $this->view->row_index = $row_index;
          $rows[$row_index] = $this->row_plugin->render($row);
          
          $map_region_key = (empty($this->options['metaregion_group']) ? $row_index : $key);
          $map_regions[$map_region_key][$row_index]['raw'] = $row;
          $map_regions[$map_region_key][$row_index]['rendered'] = $rows[$row_index];
        }
      }

      $output .= theme($this->theme_functions(),
        array(
          'view' => $this->view,
          'options' => $this->options,
          'rows' => $rows,
          'title' => $title,
          'map_regions' => $map_regions)
        );
    }
    unset($this->view->row_index);
    return $output;
  }

}
