<?php

/**
 * @file
 * The theme system, which controls the output of the map
 * 
 */

/**
 * Preprocess the primary theme implementation for a view.
 */
function template_preprocess_views_jsmap(&$vars) {
  $view = $vars['view'];

  $rows = $vars['rows'];
  $options = $vars['options'];
  $map_regions = $vars['map_regions'];
  $vars['region_content'] = array();
  
  if(!$view->editing && !empty($options['region_field'])){ // don't display the map if we're editing the view; it doesn't play nice with the ajax right now 
    if(is_array($map_regions)){
      $nids = array();
      $regions = array();
      
      foreach($map_regions as $region_title => $map_region){
        //generate the id of the div where this region's data will be stored
        $vars['ids_array'][$region_title] = 'jsmap-region-'. drupal_html_class($region_title);
        
        //region defaults
        $region = array(
          'url' => '/',
          'data_div_id' => $vars['ids_array'][$region_title],
          'name' => $region_title,
          'path' => '',
          'interactive' => MAP_VALUE_YES,
          'name_display' => MAP_VALUE_YES,
          'name_offset' => '',
          'name_use_line' => 0,
          'name_line_offset' => '',
        ); 
        
        //cycle through interior regions
        foreach($map_region as $key => $result){
          if(!empty($result['raw']->{'field_'. $options['region_field']})){
            
            //load this region's data from the field
            
            $temp_region = entity_load('jsmap_region', $result['raw']->{'field_'. $options['region_field']}[0]['raw']);
            $temp_region = (array) reset($temp_region);
            //$temp_region = $result['raw']->{'field_'. $options['region_field']}[0]['raw'];//@todo handle multiple values properly
            
            //if there is no metaregion configured then we can pull all the config for this region from the field
            if(empty($options['metaregion_group'])){
              $region = array_merge($region,$temp_region);
              $region['url'] = '/node/'. $result['raw']->nid;
              if(!empty($result['raw']->{'field_'. $options['custom_color_field']})) $region['fill'] = drupal_render($result['raw']->{'field_'. $options['custom_color_field']}[0]['rendered']);//@todo security audit and should we be grabbing the rendered version of this var?
            }
            
            //concatenate the path data
            $region['path'] .= $temp_region['svg_path'];
          }
        }
        
        $region = (object) $region;
        $regions[] = $region;
        
        $vars['region_content'][$region_title] = theme('jsmap_region', array('map_name' => 'views-'. $view->name .'-'. $view->current_display, 'region_title' => $region_title, 'records' => $map_region));
      }
    }
    
    $vars['jsmap'] = theme('jsmap', array(
      'jsmap' => NULL,
      'options' => array(
      	'general' => array(
          'fontSize' => $options['label_size'],
          'scale' => $options['scale'],
          'max_width' => $options['max_width'],
    	  'max_height' => $options['max_height'],
          'showNames' => $options['display_labels'],
        ),
      	'pathDefaults' => array(
          'interactive' => $options['interactive'],
          'fill' => $options['region_color'],
          'fill-hover' => $options['region_hover_color'],
          'stroke' => $options['stroke_color'],
          'stroke-width' => $options['stroke_width']
        )
      ),
      'regions' => $regions,
      )
    );
  }
  else{
    $vars['jsmap'] = array('#markup' => 'This display format can not be previewed');
  }
}

function theme_jsmap_region($vars){
  $output = '';
  foreach ($vars['records'] as $id => $row){
    $output .= '<div>'. $row['rendered'] .'</div>';
  }
  return $output;
}
